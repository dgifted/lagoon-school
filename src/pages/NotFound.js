import React from 'react';
import {Link} from 'react-router-dom';

function NotFound() {
    return (
        <div
            className={`d-flex flex-column justify-content-center align-items-center overlaid`}
        >
            <h1 className={''} style={{
                fontSize: '10rem'
            }}>404</h1>
            <p className={'justify-self-end'}>
                Take me <Link to={'/'} className={'link-button'}>Back</Link>
            </p>
        </div>
    );
}

export default NotFound;
