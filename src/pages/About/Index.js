import {extractActiveLinkText} from "../../misc/utils";
import {navMenu} from "../../misc/staic-contents";
import {Col, Row} from "react-bootstrap";
import BannerImage from "../../assets/images/img_14-sm.png";
import FooterImage from "../../assets/images/img_13-sm.png";
import Layout from "../../ui/Layout";
import React from "react";
import {useLocation} from "react-router-dom";
import classes from './Index.module.css';
import MoveToTop from "../../ui/MoveToTop";
import ExploreBanner1 from '../../assets/images/img_7-sm.png';


const Index = () => {
    const loc = useLocation();

    return (
        <>
            <Layout activeLinkText={extractActiveLinkText(loc.pathname, navMenu.links.about)}
                    bannerImage={BannerImage}
                    footerImage={FooterImage}
                    navLinks={navMenu.links.about}
                    parentLinkText={'About'}>
                <p className={classes['intro-text']}>
                    The Lagoon School is the first project of the Nigerian Association for Women`s Advancement (NAWA), a
                    not-for-profit and non-governmental educational and social trust dedicated to investing in the girl
                    child for the good of the society.
                </p>

                <p className={classes['regular-text']}>The Lagoon School started as a secondary school open only to
                    girls on 15th September, 1995 with only
                    fifty students. It was situated at number 75 Adisa Bashua Street in Surulere, Lagos. We moved to our
                    permanent site in Lekki in April, 2001, with four hundred and nineteen students. Right now, The
                    Lagoon School has a population of One thousand and thirty four students. This school is in response
                    to the high demand for a school that upholds virtues and creates an avenue where teachers can
                    collaborate with parents in their most important role of educating their children.</p>

                <p><img src={BannerImage} className={'w-100 my-5'}/></p>

                <p className={classes['regular-text']}>It is a school founded on the inspirations and teachings of St.
                    JoseMaria (Founder of Opus Dei) who believed that parents are the primary educators of their
                    children. He also believed that teachers
                    were to work with parents in every phase of their education, giving due consideration to the
                    differences in gender while not forgetting the proper ends Divine Providence assigns to each child
                    in the family and society.</p>

                <Row>
                    <Col sm={12} md={6}>
                        <p className={classes['regular-text']}>
                            NAWA as the name implies, is concerned with the female-child. In Lagoon school, we open
                            opportunities for our students to cultivate an optimistic approach in creating and fostering
                            relationships in a humane society where each person is loved for who she is. Our students
                            have a deep understanding of their dignity as women and their roles in the society.
                        </p>

                        <p className={classes['regular-text']}>The primary section of The Lagoon School was set up on
                            15th September, 2008 with the main aim
                            of giving an all-round education to the female child right from the pre-primary stage. It’s
                            academic session started in 2009 on Akanbi Disu street, in Lekki Phase 1 with one pupil in
                            Reception class and eleven students in Year 1. The primary section, which is located within
                            the serene premises of the Secondary School, moved to its permanent site on 14th January,
                            2012.</p>
                    </Col>
                    <Col sm={12} md={6}>
                        <img src={ExploreBanner1} alt={''} className={'img-fluid'}/>
                    </Col>
                </Row>
                <p className={classes['regular-text']}>NAWA has entrusted the spiritual, doctrinal and moral
                    formation of the students, staff and parents of the school to Opus Dei, a personal prelature
                    in the Catholic Church. Thus the spiritual, doctrinal and moral formation given in the
                    school is Catholic oriented. Opus Dei is Latin for ‘work of God’. It is “a Catholic
                    institution founded by Saint Josemaría Escrivá. Its mission is to spread the message that
                    work and the circumstances of everyday life are occasions for growing closer to God, for
                    serving others, and for improving society.”</p>

                <p className={classes['regular-text']}>The message of Opus Dei is sanctification of work. It is to
                    convert our daily noble activities into sacrifice offered to God. It has to do with working well and
                    putting in ones best. The prelature, however does not assume legal responsibility for the school. We
                    groom our students to always give their best. Their professional work in school is to study.Our
                    Christian identity is incorporated in every aspect of our curriculum. Our students, right from the
                    primary section, learn to serve God out of love by serving their fellow men. They do not just know
                    what it is to be good, they practice it by being charitable to those they come across in their
                    day-to-day activities. Thus our curriculum is such that our students are helped to understand their
                    roles in transforming our great country, Nigeria and the world as a whole.</p>

            </Layout>

            <MoveToTop/>
        </>
    );
};

export default Index;
