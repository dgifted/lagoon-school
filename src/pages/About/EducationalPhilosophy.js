import React from "react";
import MoveToTop from "../../ui/MoveToTop";
import {useLocation} from "react-router-dom";
import BannerImage from "../../assets/images/img_16-sm.png";
import FooterImage from "../../assets/images/img_17-sm.png";
import classes from './Index.module.css';
import Layout from "../../ui/Layout";
import {extractActiveLinkText} from "../../misc/utils";
import {navMenu} from "../../misc/staic-contents";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faPlay} from "@fortawesome/free-solid-svg-icons/faPlay";


const EducationalPhilosophy = () => {
    const loc = useLocation();

    return (
        <>
            <Layout activeLinkText={extractActiveLinkText(loc.pathname, navMenu.links.about)}
                    bannerImage={BannerImage}
                    footerImage={FooterImage}
                    navLinks={navMenu.links.about}
                    parentLinkText={'About'}>
                <p className={classes['intro-text']}>
                    The Lagoon School is open to girls of all cultural, religious and ethnic backgrounds. Our
                    educational model is based on our mission statement: Partnership with parents to give an all-round
                    education to each child based on the dignity of the human person, integrity, leadership qualities
                    and academic excellence.
                </p>

                <p className={`${classes['regular-text']} mb-5`}>The Lagoon School believes that education is not just about learning subjects but also about learning
                    the value of being a good person. We build characters, form and develop the total person. Thus our
                    curriculum is geared towards developing the intellectual, physiological, psychological, ethical,
                    social and spiritual dimensions of the learners. Our students have learnt to take responsibility for
                    their learning. Each child is empowered to achieve the best in whatever task she engages in. Our
                    driving force is a desire to achieve a Christian identity for the school.</p>

                <div className={classes['collapsible-section']}>
                    <FontAwesomeIcon icon={faPlay} size={'sm'} className={'mr-2'}/>
                    <span>Mission</span>
                </div>

                <div className={classes['collapsible-section']}>
                    <FontAwesomeIcon icon={faPlay} size={'sm'} className={'mr-2'}/>
                    <span>Vision</span>
                </div>

                <div className={classes['collapsible-section']}>
                    <FontAwesomeIcon icon={faPlay} size={'sm'} className={'mr-2'}/>
                    <span>Values</span>
                </div>
            </Layout>

            <MoveToTop/>
        </>
    );
};

export default EducationalPhilosophy;
