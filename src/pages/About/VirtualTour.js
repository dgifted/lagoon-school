import React, {useEffect, useState} from "react";
import MoveToTop from "../../ui/MoveToTop";
import BannerImage from "../../assets/images/img_20-sm.png";
import FooterImage from "../../assets/images/img_21-sm.png";
import Layout from "../../ui/Layout";
import {useLocation} from "react-router-dom";
import {extractActiveLinkText} from "../../misc/utils";
import {navMenu} from "../../misc/staic-contents";
import UnderConstruction from "../../components/UnderConstruction";
import {api} from "../../misc/apis";
import SuspenseFallback from "../../components/SuspenseFallback";

const VirtualTour = () => {
    const loc = useLocation();
    const [isLoading, setIsLoading] = useState(true);
    const [pageData, setPageData] = useState(null);
    const pageTitle = !pageData ? extractActiveLinkText(loc.pathname, navMenu.links.about)  : pageData?.title;

    const fetchContent = () => {
        api.get('about/virtual-tour')
            .then(res => {
                console.log('from virtual tour', res);
                setPageData(res.data);
                setIsLoading(false);
            })
            .catch(err => {
                console.log('from virtual tour', err.response);
                setIsLoading(false);
            });
    };

    useEffect(() => {
        fetchContent();
    }, []);

    let pageContent;
    if (isLoading) {
        pageContent = <SuspenseFallback/>;
    }
    if ((!isLoading && !pageData) || !pageData?.content) {
        pageContent = <UnderConstruction/>;
    }
    if (pageData && pageData?.content) {
        pageContent = (
            <>
                <div dangerouslySetInnerHTML={{__html: pageData.content}}/>
            </>
        );
    }

    return (
        <>
            <Layout activeLinkText={pageTitle}
                    bannerImage={BannerImage}
                    footerImage={FooterImage}
                    navLinks={navMenu.links.about}
                    parentLinkText={'About'}>
                <div style={{minHeight: '50vh'}}>
                    {pageContent}
                </div>
            </Layout>
            <MoveToTop/>
        </>
    );
};

export default VirtualTour;
