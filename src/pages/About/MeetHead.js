import React from "react";
import {useLocation} from "react-router-dom";
import BannerImage from "../../assets/images/img_11-sm.png";
import FooterImage from "../../assets/images/img_12-sm.png";
import HeadMistress from '../../assets/images/img_15-sm.png';
import Layout from "../../ui/Layout";
import MoveToTop from "../../ui/MoveToTop";
import {extractActiveLinkText} from "../../misc/utils";
import {navMenu} from "../../misc/staic-contents";
import {Row, Col} from "react-bootstrap";

import classes from './Index.module.css';

const MeetHead = () => {
    const loc = useLocation();

    return (
        <>
            <Layout activeLinkText={extractActiveLinkText(loc.pathname, navMenu.links.about)}
                    bannerImage={BannerImage}
                    footerImage={FooterImage}
                    navLinks={navMenu.links.about}
                    parentLinkText={'About'}>
                <Row>
                    <Col sm={12} md={4}>
                        <img src={HeadMistress} alt={'Head of school'} className={'img-fluid'}/>
                    </Col>

                    <Col sm={12} md={8}>
                        <p className={classes['regular-text']}>
                            Welcome to Our Great School!. <br/><br/>
                            The Lagoon School aims at investing in the Nigerian girl child
                            for the good of the society. We have both primary and secondary sections. Our school has a
                            reputation of high moral and academic standards. We have been able to achieve these through
                            our mission: ‘partnership with the parents to give an all-round education to the students,
                            based on the dignity of the human person, integrity, leadership qualities and academic
                            excellence’ and our vision: ‘Christian Identity’.<br /><br />
                            I welcome you- parents, teachers,
                            students, prospective parents and guests- to explore our website.<br /><br />
                            To our parents: You can
                            keep up to date with your daughter’s progress in the school, be it academic or
                            extra-curricular. You also have access to schedules of interesting activities specially
                            organised for you and your children.<br /><br />
                            To our teachers: This is another great avenue for
                            getting across to parents and students and communicating with one another.<br /><br />
                            To the students:
                            I encourage you to visit the website as often as possible to get the latest updates on your
                            assignments and projects.<br /><br />
                            To prospective parents and guests: we hope that the information on
                            our website will be of interest to you and will excite you into joining this great family of
                            The Lagoon School.<br /><br />
                            We would be grateful to receive suggestions and feedback. For more
                            information, do not hesitate to contact us.<br /><br />
                            Warmly, Isebor Margaret Head of School
                        </p>
                    </Col>
                </Row>
            </Layout>

            <MoveToTop/>
        </>
    );
};

export default MeetHead;
