import React, {useEffect, useState} from "react";

import MoveToTop from "../../ui/MoveToTop";
import BannerImage from "../../assets/images/img_18-sm.png";
import FooterImage from "../../assets/images/img_19-sm.png";
import Layout from "../../ui/Layout";
import {useLocation} from "react-router-dom";
import {extractActiveLinkText} from "../../misc/utils";
import {navMenu} from "../../misc/staic-contents";
import {api} from "../../misc/apis";
import SuspenseFallback from "../../components/SuspenseFallback";
import NewsListingCard from "../../components/NewsListingCard";
import NoNewsImage from '../../assets/images/now-news.png';

const NewsUpdate = () => {
    const loc = useLocation();
    const [isLoading, setIsLoading] = useState(true);
    const [loadedNews, setLoadedNews] = useState(null);

    useEffect(() => {
        fetchNewsUpdates().then(x => x);
    }, []);

    const fetchNewsUpdates = async () => {
        try {
            const resp = await api.get('news');
            setLoadedNews(resp.data);
        } catch (err) {
            console.log(err.toString());
        }
        setIsLoading(false)
    };

    let pageContent;
    if (isLoading) {
        pageContent = <SuspenseFallback/>;
    }

    if (loadedNews && loadedNews?.length === 0) {
        pageContent = (
            <div className={'my-5 d-flex flex-column justify-content-center align-items-center'}>
                <img
                    alt={'No news'}
                    src={NoNewsImage}
                    width={200}
                />
                <h2>No news post</h2>
            </div>
        );
    }

    if (loadedNews && loadedNews?.length > 0) {
        pageContent = (
            <>
                {loadedNews?.map(news =>
                    <NewsListingCard
                        key={news.id.toString()}
                        newsItem={news}
                    />
                )}
            </>
        );
    }

    return (
        <>
            <Layout activeLinkText={extractActiveLinkText(loc.pathname, navMenu.links.about)}
                    bannerImage={BannerImage}
                    footerImage={FooterImage}
                    navLinks={navMenu.links.about}
                    parentLinkText={'About'}>
                {pageContent}
            </Layout>

            <MoveToTop/>
        </>
    );
};

export default NewsUpdate;
