import React from "react";
import Layout from "../../ui/Layout";
import {extractActiveLinkText} from "../../misc/utils";
import {navMenu} from "../../misc/staic-contents";
import BannerImage from "../../assets/images/img_24-sm.png";
import FooterImage from "../../assets/images/img_25-sm.png";
import MoveToTop from "../../ui/MoveToTop";
import {useLocation} from "react-router-dom";
import UnderConstruction from "../../components/UnderConstruction";


const ContactUs = () => {
    const loc = useLocation();

    return (
        <>
            <Layout activeLinkText={extractActiveLinkText(loc.pathname, navMenu.links.about)}
                    bannerImage={BannerImage}
                    footerImage={FooterImage}
                    navLinks={navMenu.links.about}
                    parentLinkText={'About'}>
                <div style={{minHeight: '50vh'}}>
                    <UnderConstruction/>
                </div>
            </Layout>

            <MoveToTop/>
        </>
    );
};

export default ContactUs;
