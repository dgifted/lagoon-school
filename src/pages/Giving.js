import React, {Suspense} from "react";
import {Redirect, Route, Switch, useLocation} from "react-router-dom";

import MoveToTop from "../ui/MoveToTop";
import SuspenseFallback from "../components/SuspenseFallback";

const GivingFAQ = React.lazy(() => import('./Giving/GivingFAQ'));
const HowToGive = React.lazy(() => import('./Giving/HowToGive'));
const WhyGive = React.lazy(() => import('./Giving/WhyGive'));

const Giving = () => {
    const location = useLocation();

    return (
        <>
            <Suspense fallback={<SuspenseFallback/>}>
                <Switch location={location} key={location.pathname}>
                    <Redirect exact from={'/giving'} to={'/giving/why-give'}/>
                    <Route path={'/giving/why-give'} component={WhyGive}/>
                    <Route path={'/giving/giving-faq'} component={GivingFAQ}/>
                    <Route path={'/giving/how-to-give'} component={HowToGive}/>
                    <Redirect from={'*'} to={'/404'}/>
                </Switch>
            </Suspense>

            <MoveToTop/>
        </>
    );
};

export default Giving;
