import React, {Suspense} from "react";
import {Redirect, Route, Switch, useLocation} from 'react-router-dom';
import MoveToTop from "../ui/MoveToTop";
import SuspenseFallback from "../components/SuspenseFallback";
import StickyWidget from "../components/StickyWidget";

const AcademicCalender = React.lazy(() => import('./Academics/AcademicCalender'));
const AcademicFacilities = React.lazy(() => import('./Academics/AcademicFacilities'));
const JuniorSchool = React.lazy(() => import('./Academics/JuniorSchool'));
const SeniorSchool = React.lazy(() => import('./Academics/SeniorSchool'));
const Library = React.lazy(() => import('./Academics/Library'));

const Academics = () => {
    const location = useLocation();

    return (
        <>
            <Suspense fallback={<SuspenseFallback/>}>
                <Switch location={location} key={location.pathname}>
                    <Redirect exact from={'/academics'} to={'/academics/academic-facilities'} />
                    <Route path={'/academics/academic-facilities'} component={AcademicFacilities} />
                    <Route path={'/academics/junior-school'} component={JuniorSchool} />
                    <Route path={'/academics/senior-school'} component={SeniorSchool} />
                    <Route path={'/academics/library'} component={Library} />
                    <Route path={'/academics/academic-calendar'} component={AcademicCalender} />
                    <Redirect from={'*'} to={'/404'}/>
                </Switch>
            </Suspense>

            <MoveToTop/>
        </>
    );
};

export default Academics;
