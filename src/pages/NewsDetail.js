import React, {useEffect, useState} from 'react';
import {useLocation, useParams, Link, useHistory} from 'react-router-dom';
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";

import Layout from "../ui/Layout";
import MoveToTop from "../ui/MoveToTop";
import BannerImage from "../assets/images/img_18-sm.png";
import FooterImage from "../assets/images/img_19-sm.png";
import SuspenseFallback from "../components/SuspenseFallback";
import {api} from "../misc/apis";
import {faArrowLeft} from "@fortawesome/free-solid-svg-icons/faArrowLeft";

function NewsDetail() {
    const location = useLocation();
    const {slug} = useParams();
    const [isLoading, setIsLoading] = useState(true);
    const [error, setError] = useState(null);
    const [data, setData] = useState(null);
    const history = useHistory();

    useEffect(() => {
        api.get(`news/${slug}`)
            .then(res => {
                setIsLoading(false);
                setData(res.data);
            })
            .catch(err => {
                console.log('error response -> ', err.response);
                setIsLoading(false);
                if (err.response.status === 404) {
                    history.push('/404')
                }
            });
    }, [location]);

    let pageContent;
    if (isLoading) {
        return <SuspenseFallback/>;
    }
    if (error) {
        pageContent = <div className={`d-flex align-items-center justify-content-center`}>
            <h3 className={`my-5`}>Error getting news post</h3>
        </div>
    }

    pageContent = <>
        <h3 className={'text-capitalize'}>{data?.title}</h3>
        <p dangerouslySetInnerHTML={{__html: data?.content}} />
    </>;

    return (
        <>
            <Layout
                activeLinkText={''}
                hideAside
                parentLinkText={'News & Updates'}
                bannerImage={BannerImage}
                footerImage={FooterImage}
            >
                <div className={`mb-3`}>
                    <Link to={'/about/news-update'} className={'link-button'}>
                        <FontAwesomeIcon icon={faArrowLeft}/>{' '}Back to News
                    </Link>
                </div>

                {pageContent}
            </Layout>

            <MoveToTop/>
        </>
    );
}

export default NewsDetail;
