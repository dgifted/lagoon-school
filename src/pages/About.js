import React, {Suspense} from "react";
import {Redirect, Route, Switch, useLocation} from 'react-router-dom';
import MoveToTop from "../ui/MoveToTop";
import SuspenseFallback from "../components/SuspenseFallback";

const Index = React.lazy(() => import('./About/Index'));
const MeetHead = React.lazy(() => import('./About/MeetHead'));
const EducationalPhilosophy = React.lazy(() => import('./About/EducationalPhilosophy'));
const NewsUpdate = React.lazy(() => import('./About/NewsUpdate'));
const VirtualTour = React.lazy(() => import('./About/VirtualTour'));
const ParentPartnership = React.lazy(() => import('./About/ParentPartnership'));
const ContactUs = React.lazy(() => import('./About/ContactUs'));


const About = () => {
    const location = useLocation();

    return (
        <>
            <Suspense fallback={<SuspenseFallback/>}>
                <Switch location={location} key={location.pathname}>
                    <Redirect exact from={'/about'} to={'/about/home'}/>
                    <Route path={'/about/home'} component={Index}/>
                    <Route path={'/about/meet-head'} component={MeetHead}/>
                    <Route path={'/about/educational-philosophy'} component={EducationalPhilosophy}/>
                    <Route path={'/about/news-update'} component={NewsUpdate}/>
                    <Route path={'/about/virtual-tour'} component={VirtualTour}/>
                    <Route path={'/about/parent-partnership'} component={ParentPartnership}/>
                    <Route path={'/about/contact-us'} component={ContactUs}/>
                    <Redirect from={'*'} to={'/404'}/>
                </Switch>
            </Suspense>

            <MoveToTop/>
        </>
    );
};

export default About;
