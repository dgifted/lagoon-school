import React, {Suspense} from "react";
import MoveToTop from "../ui/MoveToTop";
import SuspenseFallback from "../components/SuspenseFallback";
import {Redirect, Route, Switch, useLocation} from "react-router-dom";
import StickyWidget from "../components/StickyWidget";

const AdmissionProcedure = React.lazy(() => import('./Admission/AdmissionProcedure'));
const SchoolTuitionFees = React.lazy(() => import('./Admission/SchoolTuitionFees'));
const Scholarships = React.lazy(() => import('./Admission/Scholarships'));
const FAQS = React.lazy(() => import('./Admission/FAQS'));
const Apply = React.lazy(() => import('./Admission/Apply'));

const Admission = () => {
    const location = useLocation();

    return (
        <>
            <Suspense fallback={<SuspenseFallback/>}>
                <Switch location={location} key={location.pathname}>
                    <Redirect exact from={'/admission'} to={'/admission/admission-procedure'}/>
                    <Route path={'/admission/admission-procedure'} component={AdmissionProcedure}/>
                    <Route path={'/admission/tuition'} component={SchoolTuitionFees}/>
                    <Route path={'/admission/scholarships'} component={Scholarships}/>
                    <Route path={'/admission/faq'} component={FAQS}/>
                    <Route path={'/admission/apply'} component={Apply}/>
                    <Redirect from={'*'} to={'/404'}/>
                </Switch>
            </Suspense>

            <MoveToTop/>
        </>
    );
};

export default Admission;
