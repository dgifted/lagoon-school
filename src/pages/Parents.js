import React, {Suspense} from "react";
import {Redirect, Route, Switch, useLocation} from "react-router-dom";

// import classes from './About.module.css';
import MoveToTop from "../ui/MoveToTop";
import SuspenseFallback from "../components/SuspenseFallback";
import StickyWidget from "../components/StickyWidget";

const NAFAD = React.lazy(() => import('./Parents/NAFAD'));
const DigitalSafety = React.lazy(() => import('./Parents/DigitalSafety'));
const LunchMenu = React.lazy(() => import('./Parents/LunchMenu'));


const Parents = () => {
    const location = useLocation();

    return (
        <>
            <Suspense fallback={<SuspenseFallback/>}>
                <Switch location={location} key={location.pathname}>
                    <Redirect exact from={'/parents'} to={'/parents/nafad'}/>
                    <Route path={'/parents/nafad'} component={NAFAD} />
                    <Route path={'/parents/digital-safety'} component={DigitalSafety} />
                    <Route path={'/parents/lunch-menu'} component={LunchMenu} />
                    <Redirect from={'*'} to={'/404'}/>
                </Switch>
            </Suspense>

            <MoveToTop/>
        </>
    );
};

export default Parents;
