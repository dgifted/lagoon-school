import React, {useEffect, useState} from "react";
import {useHistory} from 'react-router-dom';
import {Carousel} from "react-bootstrap";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import Card from "react-bootstrap/Card";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import Lightbox from "react-image-lightbox";
import 'react-image-lightbox/style.css';

import classes from './Index.module.css';
import slideImages from "../misc/index-carousel-images";
import ButtonClear from "../ui/ButtonClear";
import ExploreCard from "../ui/ExploreCard";
import ExploreBanner1 from '../assets/images/img_7-sm.png';
import ExploreBanner2 from '../assets/images/img_8-sm.png';
import ExploreBanner3 from '../assets/images/img_67.png';
import ExploreBanner4 from '../assets/images/img_68.png';
import AtAGlance from "../components/AtAGlance";

import ArticleCard from "../ui/ArticleCard";
import GalleryPlaceholder from '../assets/images/gallery_placeholder.png';
import AffiliationClass from '../assets/images/affiliation_class.png';
import JoinUs from "../components/JoinUs";
import JoinUsBgImage from '../assets/images/img_10-sm.png';
import {articleContent, galleryImages} from '../misc/staic-contents'
import MoveToTop from "../ui/MoveToTop";
import {faCaretLeft} from "@fortawesome/free-solid-svg-icons/faCaretLeft";
import {faCaretRight} from "@fortawesome/free-solid-svg-icons/faCaretRight";
import {faSearchPlus} from "@fortawesome/free-solid-svg-icons/faSearchPlus";
import {api} from "../misc/apis";
import {getNewsExcerpt} from "../misc/utils";

const Index = () => {
    const history = useHistory();
    const [isGalleryOpen, setIsGalleryOpen] = useState(false);
    const [photoIndex, setPhotoIndex] = useState(0);
    const [news, setNews] = useState();

    const fetchNews = () => {
        api.get('news')
            .then(res => {
                const abridgedNews = res.data.length > 3 ? res.data.slice(0, 3) : res.data;
                setNews(abridgedNews);
            })
            .catch(console.log);
    }

    useEffect(() => {
        fetchNews();
    }, []);

    return (
        <>
            <section className={classes['app-carousel-container']}>
                <Carousel controls={false}>
                    {slideImages.map((slide) => (
                        <Carousel.Item key={slide.sn}>
                            <img src={slide.img} alt={''} className={classes['app-carousel-container__image']}/>
                        </Carousel.Item>
                    ))}

                </Carousel>
            </section>

            <section className={'my-4 container-fluid d-flex flex-column align-items-center justify-content-start'}>
                <div className={'page-main-heading-section mt-2'}>
                    <h2>Welcome to the Lagoon School</h2>
                </div>

                <p className={classes['intro-block']}>The Lagoon School aims at investing in the Nigerian girl child for
                    the good of the society. We have
                    both primary and secondary sections. Our school has a reputation of high moral and academic
                    standards. We have been able to achieve these through our mission: ‘partnership with the parents to
                    give an all-round education to the students, based on the dignity of the human person, integrity,
                    leadership qualities and academic excellence’ and our vision: ‘Christian Identity’.</p>

                <div className={'py-4'}>
                    <ButtonClear className={classes['more-button']}
                                 onClick={e => history.push('/about/meet-head')}>
                        Read more from Mrs Isebor Margaret | The school head</ButtonClear>
                </div>

                <div className={classes['values-section']}>
                    <div>
                        <h3 className={'text-capitalize'}>Mission</h3>
                        <p>Partnership with parents to give an all-round education to each student, based on Christian
                            principles, with emphasis on the dignity of the human person, integrity, leadership
                            qualities and academic excellence.</p>
                    </div>

                    <div>
                        <h3 className={'text-capitalize'}>Vision</h3>
                        <p>To see every Lagoon student equipped with an integral education which enables her to play her
                            unique role in the home, work place and the larger society.</p>
                    </div>

                    <div>
                        <h3 className={'text-capitalize'}>Core values</h3>
                        <ol>
                            <li>Freedom and responsibility</li>
                            <li>Dignity of work</li>
                            <li>Responsible use of resources</li>
                            <li>Spirit of service</li>
                            <li>Parents involvement as primary educators</li>
                        </ol>
                    </div>

                </div>
            </section>

            <section className={`${classes['darkened']} px-0
                    container-fluid d-flex flex-column align-items-center justify-content-start`}>
                <div className={'page-sub-heading-section mb-3'}>
                    <h3>Explore More</h3>
                </div>

                <div className={classes['explore-more-wrapper']}>
                    <ExploreCard image={ExploreBanner1}
                                 title={'Primary Section'}
                                 subtitle={'Reception to Year 6'}/>

                    <ExploreCard image={ExploreBanner2}
                                 title={'Senior School'}
                                 subtitle={'JS1 - SS3'}/>
                </div>
            </section>

            <section
                className={`${classes['at-a-glance']}`}>
                <div className={'page-sub-heading-section my-0'}>
                    <h3>At a Glance</h3>
                </div>

                <AtAGlance/>
            </section>

            <section className={`${classes['darkened']} my-0 py-2
                    container-fluid d-flex flex-column align-items-center justify-content-start`}
                     style={{position: 'relative'}}>
                <div className={'page-sub-heading-section mb-0'}>
                    <h3>School News and Activities</h3>
                </div>

                <Row className={`${classes['news-activities-content']} mt-2 pt-4`}>
                    <Col sm={12} md={4} className={'mb-3'}>
                        <h3 className={`${classes['section-header']} mb-3`}>News and Stories</h3>
                        <Card className={'text-center'}>
                            <Card.Body className={`d-flex flex-column align-items-center py-5`}>
                                <>
                                    <p className={classes['news-story-date']}>JUNE 28 2021</p>
                                    <h4 className={classes['news-story-headline']}>Mid-Term Break Week</h4>
                                    <hr className={classes['news-story-demacator']}/>
                                </>

                                <>
                                    <p className={classes['news-story-date']}>JULY 2 20211</p>
                                    <h4 className={classes['news-story-headline']}>School Interhouse Sports</h4>
                                    <hr className={classes['news-story-demacator']}/>
                                </>

                                <>
                                    <p className={classes['news-story-date']}>AUGUST 28 2021</p>
                                    <h4 className={classes['news-story-headline']}>Class of 21” <br/>Graduation Ceremony
                                    </h4>
                                    <hr className={classes['news-story-demacator']}/>
                                </>

                                <div className={'my-5'}>
                                    <ButtonClear className={classes['more-button']}
                                                 onClick={e => history.push('/about/news-update')}>More LAGOON SCHOOL
                                        NEWS</ButtonClear>
                                </div>
                            </Card.Body>
                        </Card>
                    </Col>

                    <Col sm={12} md={8}>
                        <h3 className={`${classes['section-header']} mb-3`}>Articles and Blog posts</h3>
                        {news && news?.length > 0 && (
                            <>
                                <div className={`d-sm-none d-md-block mr-auto`}>
                                    <ArticleCard horizontal={true}
                                                 image={`${process.env.REACT_APP_URL}/images/${news[0].thumb}`}
                                                 content={getNewsExcerpt(news[0].content)}
                                                 author={'School Admin'}
                                                 date={new Date(news[0].created_at).toDateString()}/>
                                </div>
                                {news?.length > 1 && (
                                    <Row>
                                        <Col sm={12} md={6} className={'mb-5'}>
                                            <ArticleCard
                                                image={`${process.env.REACT_APP_URL}/images/${news[1].thumb}`}
                                                content={getNewsExcerpt(news[1].content)}
                                                author={'School Admin'}
                                                date={new Date(news[1].created_at).toDateString()}/>
                                        </Col>
                                        {news[2] && (
                                            <Col sm={12} md={6} className={'mb-5'}>
                                                <ArticleCard
                                                    image={`${process.env.REACT_APP_URL}/images/${news[2].thumb}`}
                                                    content={getNewsExcerpt(news[2].content)}
                                                    author={'School Admin'}
                                                    date={new Date(news[2].created_at).toDateString()}/>
                                            </Col>
                                        )}
                                    </Row>
                                )}
                            </>
                        )}

                        {(!news && news?.length === 0 && (
                            <>
                                <div className={`d-sm-none d-md-block mr-auto`}>
                                    <ArticleCard horizontal={true}
                                                 image={ExploreBanner2}
                                                 content={articleContent}
                                                 author={'Justus Ali JSS 3'}
                                                 date={'2 July, 2021'}/>
                                </div>
                                <Row className={`mt-5`}>
                                    <Col sm={12} md={6} className={'mb-5'}>
                                        <ArticleCard image={ExploreBanner3}
                                                     content={articleContent}
                                                     author={'Justus Ali JSS 3'}
                                                     date={'2 July, 2021'}/>
                                    </Col>

                                    <Col sm={12} md={6}>
                                        {/*<div style={{marginRight: '-5px !important'}}>*/}
                                        {/*<div >*/}
                                        <ArticleCard image={ExploreBanner4}
                                                     content={articleContent}
                                                     author={'Justus Ali JSS 3'}
                                                     date={'2 July, 2021'}
                                        />
                                        {/*</div>*/}
                                    </Col>

                                </Row>
                            </>
                        ))}


                        <div className={'text-center mt-5'}>
                            <ButtonClear
                                className={classes['more-button']}
                                onClick={e => history.push('/about/news-update')}
                            >View All</ButtonClear>
                        </div>
                    </Col>
                </Row>

                <div className={classes['float-box']}>&nbsp;</div>
            </section>

            <section
                className={'px-0 mb-2 container-fluid d-flex flex-column align-items-center justify-content-start'}>
                <div className={'page-sub-heading-section mb-3'}>
                    <h3>Gallery</h3>
                </div>

                <div style={{width: '100%', overflowY: 'hidden'}} className={`${classes['gallery-content']} mt-3`}>
                    <div
                        style={{
                            background: `url(${GalleryPlaceholder}) center/cover no-repeat padding-box`
                        }}
                    />
                    <div
                        className={classes['overlay']}
                        onClick={() => setIsGalleryOpen(true)}
                    >
                        <FontAwesomeIcon icon={faCaretLeft}/>
                        <FontAwesomeIcon icon={faSearchPlus}/>
                        <FontAwesomeIcon icon={faCaretRight}/>
                    </div>
                </div>
            </section>

            <section
                className={'px-0 mt-5 container-fluid d-flex flex-column align-items-center justify-content-start'}>
                <div className={'page-sub-heading-section mb-3'}>
                    <h3>Class of 2020 Universities</h3>
                </div>

                <img src={AffiliationClass} alt={''} style={{width: '80%'}}/>
            </section>

            <section
                className={'px-0 container-fluid d-flex flex-column align-items-center justify-content-start'}>
                <JoinUs
                    backgroundImage={JoinUsBgImage}
                    heading={'Join Us'}
                    showHeading={true}
                />
            </section>

            {isGalleryOpen && (
                <Lightbox
                    mainSrc={galleryImages[photoIndex]}
                    nextSrc={galleryImages[(photoIndex + 1) % galleryImages.length]}
                    prevSrc={galleryImages[(photoIndex + galleryImages.length - 1) % galleryImages.length]}
                    onCloseRequest={() => setIsGalleryOpen(false)}
                    onMovePrevRequest={
                        () => setPhotoIndex((photoIndex + galleryImages.length - 1) % galleryImages.length)
                    }
                    onMoveNextRequest={
                        () => setPhotoIndex((photoIndex + 1) % galleryImages.length)
                    }
                />
            )}

            <MoveToTop/>
        </>
    );
};

export default Index;
