import React, {Suspense} from "react";
import {Redirect, Route, Switch, useLocation} from "react-router-dom";
import MoveToTop from "../ui/MoveToTop";
import SuspenseFallback from "../components/SuspenseFallback";
import StickyWidget from "../components/StickyWidget";


const LifeAtLagoon = React.lazy(() => import('./StudentLife/LifeAtLagoon'));
const LagoonTraditions = React.lazy(() => import('./StudentLife/LagoonTraditions'));
const StudentLeadership = React.lazy(() => import('./StudentLife/StudentLeadership'));
const Service = React.lazy(() => import('./StudentLife/Service'));
const ClubActivities = React.lazy(() => import('./StudentLife/ClubActivities'));
const MentoringTutorials = React.lazy(() => import('./StudentLife/MentoringTutorials'));

const StudentLife = () => {
    const location = useLocation();

    return (
        <>
            <Suspense fallback={<SuspenseFallback/>}>
                <Switch location={location} key={location.pathname}>
                    <Redirect exact from={'/studentLife'} to={'/studentLife/life-at-lagoon'}/>
                    <Route path={'/studentLife/life-at-lagoon'} component={LifeAtLagoon}/>
                    <Route path={'/studentLife/traditions'} component={LagoonTraditions}/>
                    <Route path={'/studentLife/leadership'} component={StudentLeadership}/>
                    <Route path={'/studentLife/service'} component={Service}/>
                    <Route path={'/studentLife/club-activities'} component={ClubActivities}/>
                    <Route path={'/studentLife/mentoring-tutorials'} component={MentoringTutorials}/>
                    <Redirect from={'*'} to={'/404'}/>
                </Switch>
            </Suspense>

            <MoveToTop/>
        </>
    );
};

export default StudentLife;
