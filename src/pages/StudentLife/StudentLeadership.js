import Layout from "../../ui/Layout";
import {extractActiveLinkText} from "../../misc/utils";
import {navMenu} from "../../misc/staic-contents";
import BannerImage from "../../assets/images/img_47-sm.png";
import FooterImage from "../../assets/images/img_48-sm.png";
import MoveToTop from "../../ui/MoveToTop";
import React, {useEffect, useState} from "react";
import {useLocation} from "react-router-dom";
import UnderConstruction from "../../components/UnderConstruction";
import {api} from "../../misc/apis";
import SuspenseFallback from "../../components/SuspenseFallback";


const StudentLeadership = () => {
    const loc = useLocation();
    const [isLoading, setIsLoading] = useState(true);
    const [pageData, setPageData] = useState(null);
    const pageTitle = !pageData ? extractActiveLinkText(loc.pathname, navMenu.links.studentLife) : pageData?.title;

    const fetchContent = () => {
        api.get('studentLife/student-leadership')
            .then(res => {
                setPageData(res.data);
                setIsLoading(false);
            })
            .catch(err => {
                console.log('from virtual tour', err.response);
                setIsLoading(false);
            });
    };

    useEffect(() => {
        fetchContent();
    }, []);

    let pageContent;
    if (isLoading) {
        pageContent = <SuspenseFallback/>;
    }
    if ((!isLoading && !pageData) || !pageData?.content) {
        pageContent = <UnderConstruction/>;
    }
    if (pageData && pageData?.content) {
        pageContent = (
            <>
                <div dangerouslySetInnerHTML={{__html: pageData.content}}/>
            </>
        );
    }

    return (
        <>
            <Layout activeLinkText={pageTitle}
                    bannerImage={BannerImage}
                    footerImage={FooterImage}
                    navLinks={navMenu.links.studentLife}
                    parentLinkText={'Student Life'}>
                <div style={{minHeight: '50vh'}}>
                    {pageContent}
                </div>
            </Layout>

            <MoveToTop/>
        </>
    );
};

export default StudentLeadership;
