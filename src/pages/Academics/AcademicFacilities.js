import {extractActiveLinkText} from "../../misc/utils";
import {navMenu} from "../../misc/staic-contents";
import BannerImage from "../../assets/images/img_26-sm.png";
import FooterImage from "../../assets/images/img_13-sm.png";
import Layout from "../../ui/Layout";
import React, {useEffect, useState} from "react";
import {useLocation} from "react-router-dom";
import MoveToTop from "../../ui/MoveToTop";
import UnderConstruction from "../../components/UnderConstruction";
import {api} from "../../misc/apis";
import SuspenseFallback from "../../components/SuspenseFallback";


const AcademicFacilities = () => {
    const loc = useLocation();
    const [isLoading, setIsLoading] = useState(true);
    const [pageData, setPageData] = useState(null);
    const pageTitle = !pageData ? extractActiveLinkText(loc.pathname, navMenu.links.academics) : pageData?.title;

    const fetchContent = () => {
        api.get('academics/facilities')
            .then(res => {
                setPageData(res.data);
                setIsLoading(false);
            })
            .catch(err => {
                console.log('from virtual tour', err.response);
                setIsLoading(false);
            });
    };

    useEffect(() => {
        fetchContent();
    }, []);

    let pageContent;
    if (isLoading) {
        pageContent = <SuspenseFallback/>;
    }
    if ((!isLoading && !pageData) || !pageData?.content) {
        pageContent = <UnderConstruction/>;
    }
    if (pageData && pageData?.content) {
        pageContent = (
            <>
                <div dangerouslySetInnerHTML={{__html: pageData.content}}/>
            </>
        );
    }

    return (
        <>
            <Layout activeLinkText={pageTitle}
                    bannerImage={BannerImage}
                    footerImage={FooterImage}
                    navLinks={navMenu.links.academics}
                    parentLinkText={'Academics'}>
                <div style={{minHeight: '50vh'}}>
                    {/*<UnderConstruction/>*/}
                    {pageContent}
                </div>
            </Layout>

            <MoveToTop/>
        </>
    );
};

export default AcademicFacilities;
