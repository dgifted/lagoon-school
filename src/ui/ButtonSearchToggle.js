import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faSearch} from "@fortawesome/free-solid-svg-icons/faSearch";

import classes from './ButtonSearchToggle.module.css';
import React from "react";

const ButtonSearchToggle = (props) => {

    return (
        <button className={classes['search-toggle-button']} onClick={props.onClick}>
            <FontAwesomeIcon icon={faSearch}/>
        </button>
    );
};

export default ButtonSearchToggle;
