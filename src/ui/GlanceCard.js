import React from "react";
import useWindowSize from "../hooks/useWindowSize";
import classes from './GlanceCard.module.css';

const GlanceCard = (props) => {
    const {width} = useWindowSize();

    return (
        <div className={classes['glance-card']}>
            {/*<img src={props.image} alt={''}/>*/}
            <div className={classes['thumbnail']} style={{
                background: `transparent url(${props.image}) center/cover no-repeat padding-box`
            }}/>

            <div className={classes['description-box']}>
                <h4>{props.title}</h4>
                <p>{props.description}</p>
            </div>
        </div>
    );
};

export default GlanceCard;
