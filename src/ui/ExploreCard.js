import classes from './ExploreCard.module.css';

const ExploreCard = (props) => {

    return (
        <div className={classes['explore-card']}>
            <img src={props.image} alt={''} className={`img-fluid`}/>
            <h4>{props.title}</h4>
            <p>{props.subtitle}</p>
        </div>
    );
};

export default ExploreCard;
