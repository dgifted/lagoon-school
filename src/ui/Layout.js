import React from "react";
import {Col, Container, Row} from "react-bootstrap";
import classes from './Layout.module.css';
import {NavLink} from "react-router-dom";
import JoinUs from "../components/JoinUs";

const Layout = ({activeLinkText, bannerImage, children, footerImage, hideAside, navLinks, parentLinkText}) => {
    const contentSpan = !hideAside ? 9 : 12;
    return (
        <>
            <section className={classes['banner-section']}>
                <div style={{background: `url(${bannerImage}) center/cover no-repeat padding-box`}}/>
            </section>

            <Container>
                <Row>
                    <Col sm={{span: 12, order: 2}} md={{span: contentSpan, order: 1}}>
                        <h2 className={classes['parent-link-text']}>{parentLinkText}</h2>
                        <h3 className={`${classes['active-link-text']} text-capitalize`}>{activeLinkText}</h3>
                        <div>&nbsp;</div>
                        <section>{children}</section>
                    </Col>

                    {!hideAside && (
                        <Col sm={{span: 12, order: 1}} md={{span: 3, order: 2}}>
                            <aside className={classes['sidenav-links']}>
                                <ul>
                                    {navLinks.map((link, idx) => (
                                        <li key={idx}>
                                            <NavLink
                                                to={link.path}
                                                activeClassName={classes['active-link']}>{link.text}</NavLink>
                                        </li>
                                    ))}
                                </ul>
                            </aside>
                        </Col>
                    )}
                </Row>
            </Container>

            <section className={`${classes['darkened']} py-5 px-0 m-0 w-100`}>
                <JoinUs backgroundImage={footerImage} showHeading={false}/>
            </section>
        </>
    );
};

export default Layout;
