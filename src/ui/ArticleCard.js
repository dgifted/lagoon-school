import Card from "react-bootstrap/Card";
import useWindowSize from "../hooks/useWindowSize";
import classes from './ArticleCard.module.css';
import React from "react";

const ArticleCard = (props) => {
    const {width} = useWindowSize();

    let orientation = 'column';

    if (props.horizontal && props.horizontal === true && width > 700) {
        orientation = 'row';
    }

    const styles = {
        display: 'flex',
        flexDirection: orientation,
        padding: '1rem 1.2rem',
        width: orientation === 'column' ? '340px' : 'auto',
        ...props.style,
    };


    if (width < 500) {
        styles.width = '350px';
    }

    let cardImgStyle = {
        marginRight: orientation === 'row' ? '25px' : '0',
        // width: '250px',
        height: orientation === 'row' ? '230px' : '250px',
    };

    if (width < 450) {
        cardImgStyle = {marginRight: '0'};
    }

    return (
        <Card style={styles} className={classes['article-card']}>
            {/*<img src={props.image} alt={''} style={cardImgStyle}/>*/}
            <div className={classes['thumbnail']} style={{
                ...cardImgStyle,
                background: `url(${props.image}) center/cover no-repeat padding-box`,
            }}/>
            <div className={classes['article-card-content']} style={{marginTop: orientation !== 'row' ? '25px' : '0'}}>
                <p className={'p-1'}>{props.content}</p>

                <footer className={`${classes['footer']} d-flex justify-content-between mt-2`}>
                    <span className={`text-muted`}>By {props.author}</span>
                    <span>{props.date}</span>
                </footer>
            </div>
        </Card>
    );
};

export default ArticleCard;
