import Slide1 from '../assets/images/img_1-sm.jpg';
import Slide2 from '../assets/images/img_2-sm.jpg';
import Slide3 from '../assets/images/img_3-sm.jpg';
import Slide4 from '../assets/images/img_4-sm.jpg';
import Slide5 from '../assets/images/img_5-sm.jpg';
import Slide6 from '../assets/images/img_6-sm.jpg';

const slideImages = [
    {img: Slide1, sn: 1},
    {img: Slide2, sn: 2},
    {img: Slide3, sn: 3},
    {img: Slide4, sn: 4},
    {img: Slide5, sn: 5},
    {img: Slide6, sn: 6},
];

export default slideImages;
