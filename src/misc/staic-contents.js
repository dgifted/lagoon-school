import GlanceBanner1 from '../assets/images/img_9-sm.png';
import GlanceBanner2 from '../assets/images/img_69.png';
import GlanceBanner3 from '../assets/images/img_73.png';
import GlanceBanner4 from '../assets/images/img_74.png';
import GlanceBanner5 from '../assets/images/img_72.png';
import GlanceBanner6 from '../assets/images/img_70.png';
import GlanceBanner7 from '../assets/images/img_71.png';

import Gallery1 from '../assets/images/gal_1.png';
import Gallery2 from '../assets/images/gal_2.png';
import Gallery3 from '../assets/images/gal_3.png';
import Gallery4 from '../assets/images/gal_4.png';
import Gallery5 from '../assets/images/gal_5.png';
import Gallery6 from '../assets/images/gal_6.png';
import Gallery7 from '../assets/images/gal_7.png';
import Gallery8 from '../assets/images/gal_8.png';
import Gallery9 from '../assets/images/gal_9.png';
import Gallery10 from '../assets/images/gal_10.png';
import Gallery11 from '../assets/images/gal_11.png';
import Gallery12 from '../assets/images/gal_12.png';
import Gallery13 from '../assets/images/gal_13.png';
import Gallery14 from '../assets/images/gal_14.png';

const articleContent = 'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. ' +
    'The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using ' +
    'Content here, content here\', making it look like readable English. Many desktop publishing packages and web ' +
    'page editors now use Lorem Ipsum as their default model text, and a search for \'lorem ipsum\'';

const navMenu = {
    description: 'The Lagoon School aims at investing in the Nigerian girl child for the good of the society. ' +
        'We have both primary and secondary sections. Our school has a reputation of high moral and academic ' +
        'standards.',
    links: {
        about: [
            {text: 'About The Lagoon School', path: 'home'},
            {text: 'Meet the Head', path: 'meet-head'},
            {text: 'Educational Philosophy & Model', path: 'educational-philosophy'},
            {text: 'News and Updates', path: 'news-update'},
            {text: 'Virtual Tour', path: 'virtual-tour'},
            {text: 'Partnership with Parents', path: 'parent-partnership'},
            {text: 'Contact Us', path: 'contact-us'}
        ],
        academics: [
            {text: 'Academic Facilities', path: 'academic-facilities'},
            {text: 'Junior School', path: 'junior-school'},
            {text: 'Senior School', path: 'senior-school'},
            {text: 'Library', path: 'library'},
            {text: 'Academic Calender', path: 'academic-calendar'},
        ],
        admission: [
            {text: 'Admission Procedure', path: 'admission-procedure'},
            {text: 'School Tuition Fees', path: 'tuition'},
            {text: 'Scholarships', path: 'scholarships'},
            {text: 'FAQS', path: 'faq'},
            {text: 'Apply to Lagoon School', path: 'apply'},
        ],
        faith: [
            {text: 'Opus Dei', path: '//www.opusdei.org'},
            {text: 'Chaplaincy', path: '//chaplaincy'},
            {text: 'Spiritual Development', path: '//spiritual-development'},
        ],
        studentLife: [
            {text: 'Life in Lagoon', path: 'life-at-lagoon'},
            {text: 'Lagoon Traditions', path: 'traditions'},
            {text: 'Student Leadership', path: 'leadership'},
            {text: 'Service', path: 'service'},
            {text: 'Club and Activities', path: 'club-activities'},
            {text: 'Mentoring/Tutorials', path: 'mentoring-tutorials'},
        ],
        giving: [
            {text: 'Why Give', path: 'why-give'},
            {text: 'Giving FAQs', path: 'giving-faq'},
            {text: 'How to Give', path: 'how-to-give'}
        ],
        parents: [
            {text: 'NAFAD', path: 'nafad'},
            {text: 'Digital Safety', path: 'digital-safety'},
            {text: 'Lunch Menu', path: 'lunch-menu'}
        ],
    }
};

const atAGlanceContent = [
    {
        image: GlanceBanner1,
        title: '100%',
        description: 'students receive personalized support, through the tutorial system.'
    },
    {
        image: GlanceBanner2,
        title: '100%',
        description: 'Students receive personalized support, through the tutorial system.'
    },
    {
        image: GlanceBanner3,
        title: '100%',
        description: 'Students participating in Club and Extra curricular activities.'
    },
    {
        image: GlanceBanner4,
        title: '15+',
        description: 'Club activities and Extra-curricular activities.'
    },
    {
        image: GlanceBanner5,
        title: 'Faith',
        description: 'Inspired by the teachings of the Catholic church and the spirit of Opus Dei.'
    },
    {
        image: GlanceBanner6,
        title: '100%',
        description: 'Partnership with parents'
    },
    {
        image: GlanceBanner7,
        title: '98%',
        description: 'Pass rate to universities locally and abroad.'
    },


];

const galleryImages = [
    Gallery1, Gallery2, Gallery3, Gallery4, Gallery5, Gallery6, Gallery7, Gallery8, Gallery9, Gallery10, Gallery11,
    Gallery12, Gallery13, Gallery14,
];

export {articleContent, navMenu, atAGlanceContent, galleryImages}
