export const extractActiveLinkText = (currentPath, targetLinkArray) => {
    const path = currentPath.split('/')[2];
    const link = targetLinkArray.find(item => item.path === path)

    return link.text || null;
};

export const getNewsExcerpt = (content) => {
    const spl = content.split(' ', 100).join(' ');
    return spl.replace(/(<([^>]+)>)/gi, "")
};
