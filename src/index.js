import React from 'react';
import ReactDOM from 'react-dom';
import {BrowserRouter} from 'react-router-dom';
import 'bootstrap/dist/css/bootstrap.css';
// import {QueryClient, QueryClientProvider} from 'react-query';
import './index.css';
import App from './App';

// const queryClient = new QueryClient();

ReactDOM.render(
  <React.StrictMode>
      <BrowserRouter>
          {/*<QueryClientProvider clent={queryClient}>*/}
          <App />
          {/*</QueryClientProvider>*/}
      </BrowserRouter>
  </React.StrictMode>,
  document.getElementById('root')
);
