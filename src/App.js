import React, {Suspense} from 'react';
import {Redirect, Route, Switch, useLocation} from 'react-router-dom';

import './App.css';
import SuspenseFallback from "./components/SuspenseFallback";
import Navbar from "./components/Navbar";
import Footer from "./components/Footer";
import ButtonToTop from "./components/ButtonToTop";
import StickyWidget from "./components/StickyWidget";

// Lazyloaded page components
const Index = React.lazy(() => import('./pages/Index'));
const About = React.lazy(() => import('./pages/About'));
const Academics = React.lazy(() => import('./pages/Academics'));
const Admission = React.lazy(() => import('./pages/Admission'));
const Faith = React.lazy(() => import('./pages/Faith'));
const StudentLife = React.lazy(() => import('./pages/StudentLife'));
const Giving = React.lazy(() => import('./pages/Giving'));
const Parents = React.lazy(() => import('./pages/Parents'));
const NewsDetail = React.lazy(() => import('./pages/NewsDetail'));
const NotFound = React.lazy(() => import('./pages/NotFound'));

function App() {
    const location = useLocation();

    return (
        <>
            <Navbar/>
            <Suspense fallback={<SuspenseFallback/>}>
                <Switch location={location} key={location.pathname}>
                    <Route path={'/'} component={Index} exact/>
                    <Route path={'/about'} component={About}/>
                    <Route path={'/academics'} component={Academics}/>
                    <Route path={'/admission'} component={Admission}/>
                    <Route path={'/faith'} component={Faith}/>
                    <Route path={'/studentLife'} component={StudentLife}/>
                    <Route path={'/giving'} component={Giving}/>
                    <Route path={'/parents'} component={Parents}/>
                    <Route path={'/news/:slug'} component={NewsDetail}/>
                    <Route path={'/404'} component={NotFound}/>
                    <Redirect from={'*'} to={'/404'}/>
                </Switch>
            </Suspense>
            <Footer/>

            <StickyWidget/>
            <ButtonToTop/>
        </>
    );
}

export default App;
