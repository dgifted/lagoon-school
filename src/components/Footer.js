import React, {useEffect, useState} from "react";
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import classes from './Footer.module.css';
import Logo from '../assets/images/logo-sm.png';
import Facebook from '../assets/images/facebook.svg';
import Instagram from '../assets/images/instagram.svg';
import Twitter from '../assets/images/twitter.svg';
import Educare from '../assets/images/educare.png';
import ASIA from '../assets/images/aisa-logo-bl.svg';
import AISEN from '../assets/images/aisen.png';
import ALLIANCE from '../assets/images/alliance.svg';
import COBI from '../assets/images/cobi.svg';
import CAMBRIDGE from '../assets/images/cambridge.svg';
import {api} from "../misc/apis";


const footerLinks = [
    {label: 'NAWA', path: '//www.nawa.org.ng'},
    {label: 'Opus Dei', path: '//www.opusdei.org.ng'},
    {label: 'Roseville Schools', path: 'https://rosevilleschool.org/'},
    {label: 'Whitesands School', path: '//www.whitesands.org.ng'},
    {label: 'Elara Study Centre', path: '//www.elarastudycentre.com'},
    {label: 'Wavecrest Study Centre', path: '//www.wavecreststudycentre.com'},
];

const Footer = () => {
    const [displaySponsors, setDisplaySponsors] = useState(true);
    const [sponsors, setSponsors] = useState();

    useEffect(() => {
        api.get('site-settings')
            .then(res => {
                if (+res.data.display_sponsors === 1) {
                    setDisplaySponsors(true);
                    fetchSponsors();
                } else {
                    setDisplaySponsors(false);
                }
            })
            .catch(console.log);
    }, []);

    const fetchSponsors = () => {
        api.get('sponsors')
            .then(res => {
                const data = res.data;
                const filteredSponsors = data.filter(s => s.image !== null);
                setSponsors(filteredSponsors);
            })
            .catch(console.log)
    }

    return (
        <section className={classes['wrapper']}>
            <div className={classes['top-section']}>
                <Row style={{width: '100%', padding: '2rem 3rem'}}>
                    <Col sm={12} md={4}>
                        <div className={classes['logo-wrapper']}>
                            <img src={Logo} alt={'Site logo'}/>
                        </div>
                        <p>Ladipo Omotesho Cole Street, Off Adewunmi Adebimpe Drive,Lekki Phase 1, Lekki - Epe
                            Expressway Lagos P.O Box 71166 Victoria Island Nigeria</p>
                        <p>Primary Section (+234) 01 3426109<br/>
                            Secondary Section (+234) 704 442 7923</p>
                    </Col>
                    <Col sm={12} md={4}>
                        <h3>Useful links</h3>
                        <ul className={classes['useful-links']}>
                            {footerLinks.map((link, idx) => (
                                <li key={idx}>
                                    <a href={link.path} target={'_blank'} rel="noreferrer">{link.label}</a>
                                </li>
                            ))}
                        </ul>
                    </Col>
                    <Col sm={12} md={4}>
                        <h3>Let's keep you up to date<br/>with our latest news.</h3>

                        <div className={'d-flex flex-column justify-content-start'} style={{
                            minHeight: '200px',
                            marginTop: '-1rem'
                        }}>
                            <div className={classes['news-letter-group']}>
                                <input type={'email'} placeholder={'Enter your email'}/>
                                <button type={'button'}>Submit</button>
                            </div>

                            <div className={'d-flex flex-column justify-content-center'}>
                                <h3 className={'my-2'}>Follow us</h3>
                                <div className={'my-3'}>
                                    <a href="https://www.facebook.com/pages/Lagoon%20School%20lekki%20Lagos/165376787575093/"
                                       style={{marginLeft: '-12px'}}
                                       target={'_blank'} rel={'noreferrer'}>
                                        <img src={Facebook} alt={'Facebook link'}/>
                                    </a>

                                    <a href="https://www.instagram.com/thelagoonschool/" target={'_blank'}
                                       rel={'noreferrer'}>
                                        <img src={Instagram} alt={'Instagram link'}/>
                                    </a>

                                    <a href="https://twitter.com" target={'_blank'} rel={'noreferrer'}>
                                        <img src={Twitter} alt={'Twitter link'}/>
                                    </a>
                                </div>
                            </div>

                            <div className={'d-none'}>
                                <a href="https://educare.school" target={'_blank'} rel={'noreferrer'}>
                                    <img src={Educare} alt={'Educare link'} style={{height: '60px'}}/>
                                </a>
                            </div>
                        </div>
                    </Col>
                </Row>
            </div>

            {displaySponsors && (
                <div className={classes['affiliation-section']}>
                    <span className={classes['affiliation-section-label']}>Affiliations</span>
                    {(!sponsors || sponsors?.length === 0) && (
                        <>
                            <img src={ASIA} alt={'asia'}/>
                            <img src={AISEN} alt={'aisen'}/>
                            <img src={ALLIANCE} alt={'alliance'}/>
                            <img src={COBI} alt={'cobi'}/>
                            <img src={CAMBRIDGE} alt={'cambridge'}/>
                        </>
                    )}

                    {sponsors?.length > 0 && (
                        <>
                            {sponsors.map(sponsor => (
                                <img
                                    key={sponsor.id.toString()}
                                    src={`${process.env.REACT_APP_URL}/images/${sponsor.image}`}
                                    alt={sponsor.name}
                                />
                            ))}
                        </>
                    )}
                </div>
            )}

            <div className={classes['copyright-section']}>
                <span>© 2021 The Lagoon School</span>
                <span>Contact us | Privacy policy</span>
            </div>
        </section>
    );
};

export default Footer;
