import React, {useState} from 'react';
import {Link, NavLink} from "react-router-dom";
import ButtonIcon from '../assets/images/Icon_feather-user.png'
import Logo from '../assets/images/logo-sm.png';
import ButtonSearchToggle from "../ui/ButtonSearchToggle";
import ButtonMenuToggle from "../ui/ButtonMenuToggle";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import classes from './Navbar.module.css';
import {faUser} from "@fortawesome/free-solid-svg-icons/faUser";
import {navMenu} from "../misc/staic-contents";
import NavOverlay from "./NavOverlay";
import {faEnvelope} from "@fortawesome/free-regular-svg-icons/faEnvelope";

const Navbar = () => {
    let overlayTimer = undefined;

    const [isExpanded, setIsExpanded] = useState(false);
    const [isSearchVisible, setIsSearchVisible] = useState(false);
    const [isMenuOverlayShown, setIsMenuOverlayShown] = useState(false);
    const [overlayContent, setOverlayContent] = useState(<p>No link for the selected menu item</p>);


    const menuToggleClickHandler = () => setIsExpanded(prevState => !prevState);
    const closeNavDrawer = () => setIsExpanded(false);
    const mobileSearchToggler = () => setIsSearchVisible(prevState => !prevState);
    const closeSearchBox = () => setIsSearchVisible(false);

    const extractLinksFromData = (linkLabel) => {
        const fChar = linkLabel.slice(0, 1);
        const lChar = linkLabel.slice(1).toLowerCase();
        linkLabel = `${fChar}${lChar}`;

        switch (linkLabel) {
            case 'About':
                return navMenu.links.about.map((menu, idx) => (
                    <Link key={idx}
                          to={`/about/${menu.path}`}
                          onClick={() => setIsMenuOverlayShown(false)}>{menu.text}</Link>
                ));
            case 'Academics':
                return navMenu.links.academics.map((menu, idx) => (
                    <Link key={idx} to={`/academics/${menu.path}`}
                          onClick={() => setIsMenuOverlayShown(false)}>{menu.text}</Link>
                ));
            case 'Admission':
                return navMenu.links.admission.map((menu, idx) => (
                    <Link
                        key={idx}
                        to={`/admission/${menu.path}`}
                        onClick={() => setIsMenuOverlayShown(false)}>{menu.text}</Link>
                ));
            case 'Faith':
                return navMenu.links.faith.map((menu, idx) => (
                    <a
                        key={idx}
                        href={`${menu.path}`}
                        target={'_blank'}
                        onClick={() => setIsMenuOverlayShown(false)}
                        rel="noreferrer">
                        {menu.text}
                    </a>
                ));
            case 'Student life':
                return navMenu.links.studentLife.map((menu, idx) => (
                    <Link
                        key={idx}
                        to={`/studentLife/${menu.path}`}
                        onClick={() => setIsMenuOverlayShown(false)}>{menu.text}</Link>
                ));
            case 'Giving':
                return navMenu.links.giving.map((menu, idx) => (
                    <Link
                        key={idx}
                        to={`/giving/${menu.path}`}
                        onClick={() => setIsMenuOverlayShown(false)}>{menu.text}</Link>
                ));
            case 'Parents':
                return navMenu.links.parents.map((menu, idx) => (
                    <Link
                        key={idx}
                        to={`/parents/${menu.path}`}
                        onClick={() => setIsMenuOverlayShown(false)}>{menu.text}</Link>
                ));
            default:
                return;
        }
    };

    const populateNavLinks = (linkLabel) => {
        const fChar = linkLabel.slice(0, 1);
        const lChar = linkLabel.slice(1).toLowerCase();
        linkLabel = `${fChar}${lChar}`;

        console.log('linkLabel ', (linkLabel));
        return extractLinksFromData(linkLabel);
    };

    const onOverLay = (eventTargetInnerText) => {
        const firstChar = eventTargetInnerText.slice(0, 1);
        const otherChars = eventTargetInnerText.slice(1).toLowerCase();
        eventTargetInnerText = `${firstChar}${otherChars}`;
        switch (eventTargetInnerText) {
            case 'About':
                return true;
            case 'Academics':
                return true;
            case 'Admission':
                return true;
            case 'Faith':
                return true;
            case 'Student life':
                return true;
            case 'Giving':
                return true;
            case 'Parents':
                return true;
            default:
                return false;
        }
    }

    const onOverlayHover = (e) => {
        if (isMenuOverlayShown) {
            if (overlayTimer) {
                clearTimeout(overlayTimer);
            }
            setIsMenuOverlayShown(false);
        }

        setIsMenuOverlayShown(true);
        if (onOverLay(e.target.innerText.trim())) {
            setOverlayContent(populateNavLinks(e.target.innerText.trim()));
        }
    }

    const onOverlayLeave = () => {
        overlayTimer = setTimeout(() => {
            setIsMenuOverlayShown(false);
        }, 200);
    };

    return (
        <>
            <div className={classes['contact-bar']}>
                <a
                    href={'mailto:info@lagoonschool.com.ng'}
                    target={'_blank'}
                    rel="noreferrer"
                >
                    <FontAwesomeIcon icon={faEnvelope}/>
                    <span className={'ml-1'}>Send us email</span>
                </a>
            </div>

            <nav className={classes['navbar-container']}>
                <div className={classes['logo-wrapper']}>
                    <Link to={'/'}>
                        <img src={Logo} alt={'Site logo'}/>
                    </Link>
                </div>
                <ul className={classes['nav-content']}>
                    <li><NavLink
                        to={'/about'}
                        activeClassName={classes['active-link']}
                        onMouseEnter={onOverlayHover}
                        onMouseLeave={onOverlayLeave}>About</NavLink></li>
                    <li><NavLink
                        to={'/academics'}
                        activeClassName={classes['active-link']}
                        onMouseEnter={onOverlayHover}
                        onMouseLeave={onOverlayLeave}>Academics</NavLink></li>
                    <li><NavLink
                        to={'/admission'}
                        activeClassName={classes['active-link']}
                        onMouseEnter={onOverlayHover}
                        onMouseLeave={onOverlayLeave}>Admission</NavLink></li>
                    <li><NavLink
                        to={'/faith'}
                        activeClassName={classes['active-link']}
                        onMouseEnter={onOverlayHover}
                        onMouseLeave={onOverlayLeave}>Faith</NavLink></li>
                    <li><NavLink
                        to={'/studentLife'}
                        activeClassName={classes['active-link']}
                        onMouseEnter={onOverlayHover}
                        onMouseLeave={onOverlayLeave}>Student life</NavLink>
                    </li>
                    <li><NavLink
                        to={'/giving'}
                        activeClassName={classes['active-link']}
                        onMouseEnter={onOverlayHover}
                        onMouseLeave={onOverlayLeave}>Giving</NavLink></li>
                    <li><NavLink
                        to={'/parents'}
                        activeClassName={classes['active-link']}
                        onMouseEnter={onOverlayHover}
                        onMouseLeave={onOverlayLeave}>Parents</NavLink></li>
                    <li>
                        <a href={'https://educare.school'}
                           className={classes['login-link']}
                           style={{backgroundColor: '#E21020', border: 'none'}}
                           target={'_blank'}>
                            {/*<FontAwesomeIcon icon={faUser} size={'1x'}/>*/}
                            <img src={ButtonIcon} className={'img-fluid mr-1'} alt={''} style={{height: '18px'}}/>
                            <span className={'mx-1 my-0 text-capitalize'}>Portal</span>
                        </a>
                    </li>
                </ul>
            </nav>

            <nav className={`${classes['nav-mobile']}`}>
                <div className={classes['logo-wrapper']}>
                    <Link to={'/'}>
                        <img src={Logo} alt={'Site logo'}/>
                    </Link>
                </div>
                <div className={`d-flex justify-content-end align-items-center`}>
                    <ButtonSearchToggle onClick={mobileSearchToggler}/>
                    <span/>
                    <ButtonMenuToggle isExpanded={isExpanded} clickHandler={menuToggleClickHandler}/>
                </div>
            </nav>

            {isMenuOverlayShown && (
                <NavOverlay
                    onMouseEnter={onOverlayHover}
                    onMouseLeave={onOverlayLeave}>
                    {overlayContent}
                </NavOverlay>
            )}

            <nav className={`${classes['nav-drawer']} ${isExpanded && 'nav-drawer-open'}`}>
                <ul className={classes['nav-drawer-content']}>
                    <li><NavLink to={'/about'}
                                 activeClassName={classes['active-link']}
                                 onClick={closeNavDrawer}>About</NavLink></li>
                    <li><NavLink to={'/academics'}
                                 activeClassName={classes['active-link']}
                                 onClick={closeNavDrawer}>Academics</NavLink></li>
                    <li><NavLink to={'/admission'}
                                 activeClassName={classes['active-link']}
                                 onClick={closeNavDrawer}>Admission</NavLink></li>
                    <li><NavLink to={'/faith'}
                                 activeClassName={classes['active-link']}
                                 onClick={closeNavDrawer}>Faith</NavLink></li>
                    <li><NavLink to={'/studentLife'}
                                 activeClassName={classes['active-link']}
                                 onClick={closeNavDrawer}>Student life</NavLink></li>
                    <li><NavLink to={'/giving'}
                                 activeClassName={classes['active-link']}
                                 onClick={closeNavDrawer}>Giving</NavLink></li>
                    <li><NavLink to={'/parents'}
                                 activeClassName={classes['active-link']}
                                 onClick={closeNavDrawer}>Parents</NavLink></li>
                    <a href={'https://educare.school'}
                       className={classes['login-link']}
                       style={{backgroundColor: '#E21020', border: 'none'}}
                       target={'_blank'}>
                        <FontAwesomeIcon icon={faUser} size={'1x'}/>
                        <span className={'mx-1 my-0'}>Portal</span>
                    </a>
                </ul>
            </nav>

            <div className={`${classes['mobile-search-wrapper']} ${isSearchVisible && 'search-mobile-open'}`}>
                <input type={'text'} placeholder={'Enter search term'}/>
                <button type={'button'} onClick={closeSearchBox}>Search</button>
            </div>
        </>
    );
};

export default Navbar;
