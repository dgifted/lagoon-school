import React from "react";

import classes from './SearchBarMobile.module.css';

const SearchBarMobile = () => {
    return (
        <div className={classes['mobile-search-wrapper']}>
            <input type={'text'} placeholder={'Enter search term'}/>
            <button>Search</button>
        </div>
    );
};

export default SearchBarMobile;
