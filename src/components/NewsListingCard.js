import React from 'react';
import {Link} from 'react-router-dom';

import Row from "react-bootstrap/Row";
import Col from 'react-bootstrap/Col';
import NoImage from '../assets/images/now-news.png';

function NewsListingCard({newsItem}) {
    const newsThumbnail = newsItem.thumb ? process.env.REACT_APP_URL + '/images/' + newsItem.thumb : NoImage;

    return (
        <Row>
            <Col sm={12} md={4}>
                <div
                    style={{
                        background: `url(${newsThumbnail}) center/cover no-repeat padding-box`,
                        height: '180px',
                        width: '100%'
                    }}
                />
            </Col>

            <Col sm={12} md={8}>
                <div className={`d-flex flex-column`}>
                    <h3 className={'text-danger'}>{newsItem.title}</h3>
                    <p>
                        <em>Published on: <time>{new Date(newsItem.created_at).toDateString()}</time></em>
                    </p>
                    <p>{newsItem.excerpt}</p>
                    <p>
                        <Link to={`/news/${newsItem.slug}`} className={'link-button'}>
                            Read More
                        </Link>
                    </p>
                </div>
            </Col>
        </Row>
    );
}

export default NewsListingCard;
