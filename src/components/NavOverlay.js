import classes from './NavOverlay.module.css';
import React from "react";
import {navMenu} from "../misc/staic-contents";
import MiniBanner from '../assets/images/img_67.png';

const NavOverlay = (props) => {

    return (
        <section
            className={classes['menu-overlay']}
            onMouseEnter={props.onMouseEnter}
            onMouseLeave={props.onMouseLeave}>
            <div className={classes['menu-container']}>
                <div
                    className={classes['mini-banner']}
                    style={{ background: `url(${MiniBanner}) center/cover no-repeat padding-box`}}
                    >
                    <img src={MiniBanner} alt={'Nav placeholder'}/>
                </div>

                <div className={classes['nav-description']}>{navMenu.description}</div>
                <span/>
                <div className={classes['nav-links']}>{props.children}</div>
            </div>
        </section>
    );
};

export default NavOverlay;
