import React, {useEffect, useState} from "react";
import GlanceBanner1 from '../assets/images/img_9-sm.png';
import classes from './AtAGlance.module.css';
import GlanceCard from "../ui/GlanceCard";
import arrowRightActive from '../assets/images/right-active.png';
import arrowRightInactive from '../assets/images/right-inactive.png';
import arrowLeftActive from '../assets/images/left-active.png';
import arrowLeftInactive from '../assets/images/left-inactive.png';
import { atAGlanceContent } from '../misc/staic-contents';

const AtAGlance = () => {
    const [slideCount, setSlideCount] = useState(1);
    const [activeSlide, setActiveSlide] = useState(0);

    useEffect(() => {
        setSlideCount(atAGlanceContent.length);

        return () => {
            setActiveSlide(1);
        }
    }, []);

    const pushSlide = () => {
        if (activeSlide === slideCount - 1) {
            return;
        }
        setActiveSlide(prevState => prevState + 1);
    };

    const popSlide = () => {
        if (activeSlide === 0) {
            return;
        }

        setActiveSlide(prevState => prevState - 1);
    };

    const currentSlide = atAGlanceContent[activeSlide];

    return (
        <div className={classes['wrapper']}>
            <GlanceCard image={currentSlide.image}
                        title={currentSlide.title}
                        description={currentSlide.description}/>

            <div className={classes['controls']}>
                <button onClick={popSlide} style={{
                    background: `url(${activeSlide === 0 ? arrowLeftInactive : arrowLeftActive}) center/cover no-repeat padding-box`,
                    width: '40px',
                    height: '40px',
                    cursor: `${activeSlide === 0 ? 'not-allowed' : 'pointer'}`
                }}>&nbsp;</button>

                <button onClick={pushSlide} style={{
                    background: `url(${activeSlide !== (slideCount - 1) ? arrowRightActive : arrowRightInactive}) center/cover no-repeat padding-box`,
                    width: '40px',
                    height: '40px',
                    cursor: `${activeSlide === (slideCount - 1) ? 'not-allowed' : 'pointer'}`
                }}>&nbsp;</button>
            </div>

            <ul className={classes['slide-index-indicators']}>
                {atAGlanceContent.map((item, idx) => (
                    <li className={activeSlide === (idx) ? `${classes['active']}` : ''}
                        key={idx}
                        onClick={() => setActiveSlide(idx)}>&nbsp;</li>
                ))}
            </ul>
        </div>
    );
};

export default AtAGlance;
