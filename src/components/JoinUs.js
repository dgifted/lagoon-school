import {useHistory} from 'react-router-dom';
import classes from './JoinUs.module.css'
import ButtonClear from "../ui/ButtonClear";

const JoinUs = (props) => {
    const history = useHistory();
    const joinUsBtns = [
        {
            classStyle: classes['join-section-button'],
            label: 'Inquire',
            onClick: () => history.push('/')
        },
        {
            classStyle: classes['join-section-button'],
            label: 'Apply',
            onClick: () => history.push('/admission/apply')
        },
        {
            classStyle: classes['join-section-button'],
            label: 'Visit Us',
            onClick: () => history.push('/about/contact-us')
        }
    ];

    return (
        <div className={`${classes['wrapper']}`} style={{
            background: `url(${props.backgroundImage}) center/cover transparent no-repeat border-box `
        }}>
            {props.showHeading && (
                <div className={'page-sub-heading-section my-5'}>
                    <h3 style={{color: 'white'}}>{props.heading}</h3>
                </div>
            )}

            {!props.showHeading && (
                <div style={{minHeight: '5rem'}}>&nbsp;&nbsp;</div>
            )}


            <div className={classes['action-button-group']}>
                {joinUsBtns.map((button, idx) => (
                    <ButtonClear className={button.classStyle}
                                 key={idx}
                                 onClick={button.onClick}>{button.label}</ButtonClear>
                ))}
            </div>
        </div>
    );
};

export default JoinUs;
