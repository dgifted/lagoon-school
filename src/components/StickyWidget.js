import React, {useEffect, useState} from "react";
import {useLocation, useHistory} from 'react-router-dom';

import classes from './StickyWidget.module.css';
import StickyButton1Active from '../assets/images/sticky_btn_active-1.png';
import StickyButton1AInactive from '../assets/images/sticky_btn_inactive-1.png';
import StickyButton2Active from '../assets/images/sticky_btn_active-2.png';
import StickyButton2Inactive from '../assets/images/sticky_btn_inactive-2.png';
import StickyButton3Active from '../assets/images/sticky_btn_active-3.png';
import StickyButton3Inactive from '../assets/images/sticky_btn_inactive-3.png';
import StickyButton4Active from '../assets/images/sticky_btn_active-4.png';
import StickyButton4Inactive from '../assets/images/sticky_btn_inactive-4.png';


const StickyWidget = () => {
    const loc = useLocation();
    const history = useHistory();
    const [button1, setButton1] = useState(true);
    const [button2, setButton2] = useState(true);
    const [button3, setButton3] = useState(true);
    const [button4, setButton4] = useState(true);
    const [activeLink, setActiveLink] = useState({
        active1: false,
        active2: false,
        active3: false,
        active4: false,
    });

    const toggleState1 = () => setButton1(prevState => !prevState);
    const toggleState2 = () => setButton2(prevState => !prevState);
    const toggleState3 = () => setButton3(prevState => !prevState);
    const toggleState4 = () => setButton4(prevState => !prevState);

    useEffect(() => {
        switch (loc.pathname) {
            case '/about/contact-us':
                setActiveLink({
                    active1: false,
                    active2: true,
                    active3: false,
                    active4: true,
                });
                setButton2(false);
                setButton4(false);
                break;
            case '/admission/apply':
                setActiveLink({
                    active1: false,
                    active2: false,
                    active3: true,
                    active4: false,
                });
                setButton3(false);
                break;
            default:
                setActiveLink({
                    active1: false,
                    active2: false,
                    active3: false,
                    active4: false,
                });
                setButton1(true);
                setButton4(true);
                setButton2(true);
                setButton3(true);
                break;
        }
    }, [loc.pathname]);

    return (
        <div className={classes['sticky-container']}>
            <button className={`${activeLink.active1 ? classes['active'] : ''}`}
                    onClick={_ => console.log('Button pressed')}
                    onMouseEnter={toggleState1}
                    onMouseLeave={toggleState1}>
                {button1 && <img src={StickyButton1Active} alt={''}/>}
                {!button1 && <img src={StickyButton1AInactive} alt={''}/>}
                Search
            </button>

            <button className={`${activeLink.active2 || activeLink.active4 ? classes['active'] : ''}`}
                    onClick={() => history.push('/about/contact-us')}
                    onMouseEnter={toggleState2}
                    onMouseLeave={toggleState2}>
                {button2 && <img src={StickyButton2Active} alt={''}/>}
                {!button2 && <img src={StickyButton2Inactive} alt={''}/>}
                Inquire
            </button>

            <button className={`${activeLink.active3 ? classes['active'] : ''}`}
                    onClick={() => history.push('/admission/apply')}
                    onMouseEnter={toggleState3}
                    onMouseLeave={toggleState3}>
                {button3 && <img src={StickyButton3Active} alt={''}/>}
                {!button3 && <img src={StickyButton3Inactive} alt={''}/>}
                Apply
            </button>

            <button className={`${activeLink.active2 || activeLink.active4 ? classes['active'] : ''}`}
                    onClick={() => history.push('/about/contact-us')}
                    onMouseEnter={toggleState4}
                    onMouseLeave={toggleState4}>
                {button4 && <img src={StickyButton4Active} alt={''}/>}
                {!button4 && <img src={StickyButton4Inactive} alt={''}/>}   
                <span className={'mt-1'}>Visit us</span>
            </button>
        </div>
    );
};

export default StickyWidget;
