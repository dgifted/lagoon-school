import React from "react";
import Container from "react-bootstrap/Container";
import Col from "react-bootstrap/Col";
import Row from "react-bootstrap/Row";
import Image from '../assets/images/underconstrunction.png';

const UnderConstruction = () => {

    return (
        <Container>
            <Row>
                <Col>
                    <img src={Image} alt={'Page under construction.'} className={'img-fluid'}/>
                </Col>
            </Row>
        </Container>
    )
};

export default UnderConstruction;
